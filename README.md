# **KENDARAAN**

#### **How To Install**

clone dari git kemudian jalankan ``composer update``

kemudian renamefile ``env.development`` menjadi ``.env``

#### **ENV**

Anda dapat mengubah environment sesuai dengan env mongodb anda buka file ``.env`` dan tambahkan config berikut:

| KEY              | VALUE        | DESC        |
| ---------------- | ------------ | ----------- |
| DB_CONNECTION    | mongodb      | driver db   |
| MONGODB_HOST     | localhost    | host db     |
| MONGODB_PORT     | 20000        | port db     |
| MONGODB_USERNAME | root         | username db |
| MONGODB_PASSWORD | monggo       | password db |
| MONGODB_DATABASE | db_kendaraan | nama db     |

#### TEST

Unit tested passed dapat dilihat pada directory ``test/Unit/*Test.php``

untuk menjalankan test anda dapat masuk ke terminal dan jalankan ``php artisan test``
