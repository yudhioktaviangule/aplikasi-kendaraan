<?php

namespace Tests\Unit;

use App\Domain\Dto\MobilRequest;
use App\Domain\Dto\MotorRequest;
use App\Domain\Services\KendaraanService;
use Exception;
use Tests\TestCase;

class ServiceKendaraanTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_instance()
    {

        $data = KendaraanService::getInstance();

        $this->assertTrue($data!=null);
    }
    public function test_find_all()
    {

        $data = KendaraanService::getInstance();
        $page = "2";
        $res = $data->findAll($page);

       // dd($res);
        $this->assertTrue($res->getStatus()==200,$res->getMessage());
    }
    public function test_find_one()
    {

        $data = KendaraanService::getInstance();
        $page = $data->findAll("0")->getData();
        $page = $page[0];
        $res = $data->findByID($page->_id);

       // dd($res);
        $this->assertTrue($res->getStatus()==200,$res->getMessage());
    }

    public function test_save_motor_negative()
    {
        $service = KendaraanService::getInstance();
        $data = new MotorRequest();
        $data->setTahun(2020)->setMesin("DOHC 250CC");
        $err = $service->saveMotor($data);
        $this->assertEquals(400,$err->getStatus(),$err->getMessage());
    }
    public function test_save_motor_negative_minus_numbers()
    {
        $service = KendaraanService::getInstance();
        $data = new MotorRequest();
        $data->setSuspensi("Monoshock")
                ->setTransmisi("MT")
                ->setTahun(-2020)
                ->setMesin("DOHC 250CC")

                ->setHarga(-25000000)
                ->setWarna("Black")
                ;
        $err = $service->saveMotor($data);
        $this->assertEquals(400,$err->getStatus(),$err->getMessage());
    }
    public function test_save_motor_positive()
    {
        $service = KendaraanService::getInstance();
        $data = new MotorRequest();
        $data->setSuspensi("Monoshock")
                ->setTransmisi("MT")
                ->setTahun(2020)
                ->setMesin("DOHC 250CC")

                ->setHarga(25000000)
                ->setWarna("Black")
                ;
        $err = $service->saveMotor($data);
        $this->assertEquals(200,$err->getStatus(),$err->getMessage());
    }

    public function test_save_mobil_negative()
    {
        $service = KendaraanService::getInstance();
        $data = new MobilRequest();
        $data->setTahun(2020)->setMesin("DOHC 250CC");
        $err = $service->saveMobil($data);
        $this->assertEquals(400,$err->getStatus(),$err->getMessage());
    }
    public function test_save_mobil_negative_minus_numbers()
    {
        $service = KendaraanService::getInstance();
        $data = new MobilRequest();
        $data->setTipe("Sport")
                ->setKapasitas(6)
                ->setTahun(-2020)
                ->setMesin("V12")
                ->setHarga(-25000000)
                ->setWarna("Blue Metalic");
        $err = $service->saveMobil($data);
        $this->assertEquals(400,$err->getStatus(),$err->getMessage());
    }

    public function test_save_mobil_positive()
    {
        $service = KendaraanService::getInstance();
        $data = new MobilRequest();
        $data->setTipe("Sport")
                ->setKapasitas(6)
                ->setTahun(2020)
                ->setMesin("V12")
                ->setHarga(25000000)
                ->setWarna("Blue Metalic")
                ;
        $err = $service->saveMobil($data);
        $this->assertEquals(200,$err->getStatus(),$err->getMessage());
    }
    public function test_update_mobil_Negative()
    {
        $service = KendaraanService::getInstance();
        $data = new MobilRequest();
        $data->setTipe("Sport")
                ->setKapasitas(6)
                ->setTahun(2020)
                ->setMesin("CC1000")
                ->setHarga(25000000)
                ->setWarna("Blue Metalic")
                ;
        $err = $service->updateMobil(NULL,$data);
        $this->assertEquals(400,$err->getStatus(),$err->getMessage());
    }
    public function test_update_mobil_Negative_hargaMinus_tahun_minus_kapasitas_minus()
    {
        $service = KendaraanService::getInstance();
        $data = new MobilRequest();
        $data->setTipe("Sport")
                ->setKapasitas(6)
                ->setTahun(-2020)
                ->setMesin("CC1000")
                ->setHarga(-25000000)
                ->setWarna("Blue Metalic")
                ;
        $err = $service->updateMobil('63fa174cdd180000c80076e2',$data);
        $this->assertEquals(400,$err->getStatus(),$err->getMessage());
    }
    public function test_update_mobil_negative_salahJenis()
    {
        $id='63fa1744be4f000016003142';
        $service = KendaraanService::getInstance();
        $data = new MobilRequest();
        $data->setTipe("Sport")
                ->setKapasitas(6)
                ->setTahun(2020)
                ->setMesin("CC1000")
                ->setHarga(25000000)
                ->setWarna("Blue Metalic")
                ;
        $err = $service->updateMobil($id,$data);
        $this->assertEquals(400,$err->getStatus(),$err->getMessage());
    }
    public function test_save_mobil_negative_yearGreaterThanNow()
    {
        $service = KendaraanService::getInstance();
        $data = new MobilRequest();
        $data->setTipe("Sport")
                ->setKapasitas(6)
                ->setTahun(2040)
                ->setMesin("V12")
                ->setHarga(25000000)
                ->setWarna("Blue Metalic")
                ;
        $err = $service->saveMobil($data);
        $this->assertEquals(400,$err->getStatus(),$err->getMessage());
    }

    public function test_save_motor_negative_yearGreaterThanNow()
    {
        $service = KendaraanService::getInstance();
        $data = new MotorRequest();
        $data->setSuspensi("Monoshock")
                ->setTransmisi("MT")
                ->setTahun(2050)
                ->setMesin("DOHC 250CC")

                ->setHarga(25000000)
                ->setWarna("Black")
                ;
        $err = $service->saveMotor($data);
        $this->assertEquals(200,$err->getStatus(),$err->getMessage());
    }


    public function test_delete_negative_data_not_found(){
        $service = KendaraanService::getInstance();
        $all = $service->findAll("0");
        $data = $all->getData();
        $id = $data[0]->_id;
        $test = $service->deleteData("1");
        $this->assertTrue($test->getStatus()==404,$test->getMessage());
    }
    public function test_delete_negative_id_empty(){
        $service = KendaraanService::getInstance();
        $all = $service->findAll("0");
        $data = $all->getData();
        $id = $data[0]->_id;
        $test = $service->deleteData(NULL);
        $this->assertTrue($test->getStatus()==400,$test->getMessage());
    }
    public function test_delete_positive(){
        $service = KendaraanService::getInstance();
        $all = $service->findAll("0");
        $data = $all->getData();
        $id = $data[0]->_id;
        $test = $service->deleteData($id);
        $this->assertTrue($test->getStatus()==200,$test->getMessage());
    }
}
