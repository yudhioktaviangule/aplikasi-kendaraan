<?php

namespace Tests\Unit;

use App\Domain\Entity\KendaraanBuilder;
use App\Domain\Repositories\KendaraanRepository;
use App\Utilize\ErrorException;
use Tests\TestCase;

class KendaraanDataTest extends TestCase
{
    private $test_id = "";

    public function test_positive_find(){
        try{
            $repo = KendaraanRepository::getInstance();
            $all = $repo->findAll(0,10);
            $x = $all[0];
            $data = $repo->findByID($x->getId());
            $harga  =$data->getHarga();
            $execpt = 25000000;

            $this->assertTrue($data!=null,'Data masih belum sama '.$execpt.' dan '.$harga);
        }catch(\Exception $e){
            $this->assertTrue(false,ErrorException::getResponseStatusCode($e->getMessage())->err);
        }
    }
    public function test_create_json(){
        $builder = KendaraanBuilder::builder()->harga(20000)->build();
        $expectingJSON ='{"_id":null,"tahun":null,"warna":null,"harga":20000,"jenis":null,"mesin":null,"kapasitas":null,"tipe":null,"suspensi":null,"transmisi":null}';
        $result = $builder->toJSON();
        $this->assertEquals($expectingJSON,$result);
    }
    public function test_save(){
        $builder = KendaraanBuilder::builder()->harga(20000)->build();
        $repo = KendaraanRepository::getInstance();
        $dat = $repo->save($builder);
        $expect = 20000;
        $actual = $dat->getHarga();
        $this->assertEquals($expect,$actual,"Masih Belum Sama");
    }
    public function test_negative_find(){
        try{
            $repo = KendaraanRepository::getInstance();
            $data = $repo->findByID('63f9de64951a000020005302');
            $this->assertTrue(false);
        }catch(\Exception $e){
            $this->assertTrue(true);
        }
    }

    public function test_update(){
        try{
            $repo = KendaraanRepository::getInstance();
            $all = $repo->findAll(0,10);
            $x = $all[0];
            $data = $repo->findByID($x->getId());
            $data->setTahun(2020);
            $data->setMesin("Mesin Kapal");
            $repo->update($data->getId(),$data);
            $this->assertTrue(true);
        }catch(\Exception $e){
            $this->assertTrue(false,$e->getMessage());
        }
    }
    public function test_delete(){
        try{
            $repo = KendaraanRepository::getInstance();
            $repo->delete('63fa11d54b7c000031005a92');
            $this->assertTrue(true);
        }catch(\Exception $e){
            $this->assertTrue(false,$e->getMessage());
        }
    }
}
