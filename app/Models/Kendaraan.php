<?php

namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Kendaraan extends Model
{
    use SoftDeletes;
    protected $collection = "kendaraan";
    protected $primaryKey = '_id';
    protected $dates = ['deleted_at'];
}
