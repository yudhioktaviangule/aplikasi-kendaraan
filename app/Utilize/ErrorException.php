<?php

namespace App\Utilize;
class ErrorException{

    private static $code =[
        'NF'=>404,
        'BR'=>400,
        'ISR'=>500,
        'AR'=>401,
        'FB'=>403,
    ];
    public static function createNotFound($message=""){
        return "NF:$message";
    }
    public static function createBADRequest($message=""){
        return "BR:$message";
    }
    public static function createISR($message=""){
        return "ISR:$message";
    }
    public static function createForbidden($message=""){
        return "FB:$message";
    }
    public static function createAuthRequired($message=""){
        return "AR:$message";
    }

    public static function getResponseStatusCode($message = ""){

        [$xcode,$err] = explode(":",$message);
        return json_decode(json_encode([
            'code'=>self::$code[$xcode],
            'err'=>$err,
        ]));
    }
}