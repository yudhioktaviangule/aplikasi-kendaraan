<?php

namespace App\Utilize;
class Pagination{
    private static  $INSTANCE=null;
    public static function getInstance(): Pagination {
        if(self::$INSTANCE==NULL):
            self::$INSTANCE= new Pagination();
        endif;
        return self::$INSTANCE;
    }
    public $offset = 0;
    public $limit = 0;
    public function process($rpage = '0'){
        $limit = intval(env("MONGODB_LIMIT",'10'));
        $page = intval($rpage);
        $offset = $page = 0 || $page==1 ? 0:($page-1)*$limit;
        $this->offset = $offset;
        $this->limit = $limit;
        return $this;
    }
}