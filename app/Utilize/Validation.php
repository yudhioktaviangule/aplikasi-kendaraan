<?php

namespace App\Utilize;


class Validation{
    private static  $INSTANCE=null;
    public static function getInstance(): Validation {
        if(self::$INSTANCE==NULL):
            self::$INSTANCE= new Validation();
        endif;
        return self::$INSTANCE;
    }
    public static function paramCheck($name,$value){
        if($value==null){
            throw new \Exception(ErrorException::createBADRequest("Param '$name' is empty"));
        }
    }
    public static function paramMinusCheck($name,$value=0){
        if($value==NULL){
            throw new \Exception(ErrorException::createBADRequest("Param '$name' is empty"));
        }
        if($value<0){
            throw new \Exception(ErrorException::createBADRequest("Param '$name' cannot smaller than 0"));
        }
    }

    public static function paramYearCheck($name,$value){
        if($value==NULL){
            throw new \Exception(ErrorException::createBADRequest("Param '$name' is empty"));
        }
        if($value<0){
            throw new \Exception(ErrorException::createBADRequest("Param '$name' cannot smaller than 0"));
        }
        $y= intval(date("Y"));
        if($value>$y){
            throw new \Exception(ErrorException::createBADRequest("Param '$name' cannot greater than year now"));
        }
    }

}