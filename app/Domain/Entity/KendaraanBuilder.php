<?php

namespace App\Domain\Entity;
use SerializeBuilder;



class KendaraanBuilder{
    public  $_tahun = null;
    public  $_warna = null;
    public  $_harga = null;
    public  $_jenis = null;
    public  $_mesin = null;
    public  $_kapasitas = null;
    public  $_tipe = null;
    public  $_suspensi = null;
    public  $_transmisi = null;

    public static function builder(){

        return new KendaraanBuilder();
    }

    public static function fromJSON($data):Kendaraan{
        $kendaraan = new Kendaraan();
        $kendaraan = $kendaraan->fromJSON($data);
        return $kendaraan;
    }


    public function build():Kendaraan{
        return new Kendaraan(NULL,
            $this->_tahun,
            $this->_warna,
            $this->_harga,
            $this->_jenis,
            $this->_mesin,
            $this->_kapasitas,
            $this->_tipe,
            $this->_suspensi,
            $this->_transmisi
        );
    }
    public function tahun($tahun)
    {
        $this->_tahun = $tahun;
        return $this;
    }
    public function warna($warna)
    {
        $this->_warna = $warna;
        return $this;
    }
    public function harga($harga)
    {
        $this->_harga = $harga;
        return $this;
    }


    public function jenis($jenis)
    {
        $this->_jenis = $jenis;
        return $this;
    }
    public function mesin($mesin)
    {
        $this->_mesin = $mesin;
        return $this;
    }
    public function kapasitas($kapasitas)
    {
        $this->_kapasitas = $kapasitas;
        return $this;
    }
    public function tipe($tipe)
    {
        $this->_tipe = $tipe;
        return $this;
    }
    public function transmisi($transmisi)
    {
        $this->_transmisi = $transmisi;
        return $this;
    }
    public function suspensi($suspensi)
    {
        $this->_suspensi = $suspensi;
        return $this;
    }



}



