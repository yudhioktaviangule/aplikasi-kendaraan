<?php
namespace App\Domain\Entity;
class Kendaraan{
    public $_id = "";
    public $tahun = null;
    public $warna = null;
    public $harga = null;
    public $jenis = null;
    public $mesin = null;
    public $kapasitas = null;
    public $tipe = null;
    public $suspensi = null;
    public $transmisi = null;
    public  function toJSON(){
        return json_encode($this);
    }

    public function fromJSON($json_str){
        $decode = json_decode($json_str,TRUE);
        foreach($decode as $key => $val){
            if(property_exists(__CLASS__,$key)):
                $this->{$key} = $val;
            endif;
        }
        return $this;
    }
    public function __construct(
        $id="",
        $tahun="",
        $warna="",
        $harga="",
        $jenis="",
        $mesin="",
        $kapasitas="",
        $tipe="",
        $suspensi="",
        $transmisi=""
    ) {
        $this->_id=$id;
        $this->tahun=$tahun;
        $this->warna=$warna;
        $this->harga=$harga;
        $this->jenis=$jenis;
        $this->mesin=$mesin;
        $this->kapasitas=$kapasitas;
        $this->tipe=$tipe;
        $this->suspensi=$suspensi;
        $this->transmisi=$transmisi;
    }


    /**
     * Get the value of _id
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the value of _id
     */
    public function setId($_id): self
    {
        $this->_id = $_id;

        return $this;
    }

    /**
     * Get the value of tahun
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set the value of tahun
     */
    public function setTahun($tahun): self
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get the value of warna
     */
    public function getWarna()
    {
        return $this->warna;
    }

    /**
     * Set the value of warna
     */
    public function setWarna($warna): self
    {
        $this->warna = $warna;

        return $this;
    }

    /**
     * Get the value of harga
     */
    public function getHarga()
    {
        return $this->harga;
    }

    /**
     * Set the value of harga
     */
    public function setHarga($harga): self
    {
        $this->harga = $harga;

        return $this;
    }

    /**
     * Get the value of jenis
     */
    public function getJenis()
    {
        return $this->jenis;
    }

    /**
     * Set the value of jenis
     */
    public function setJenis($jenis): self
    {
        $this->jenis = $jenis;

        return $this;
    }

    /**
     * Get the value of mesin
     */
    public function getMesin()
    {
        return $this->mesin;
    }

    /**
     * Set the value of mesin
     */
    public function setMesin($mesin): self
    {
        $this->mesin = $mesin;

        return $this;
    }

    /**
     * Get the value of kapasitas
     */
    public function getKapasitas()
    {
        return $this->kapasitas;
    }

    /**
     * Set the value of kapasitas
     */
    public function setKapasitas($kapasitas): self
    {
        $this->kapasitas = $kapasitas;

        return $this;
    }

    /**
     * Get the value of tipe
     */
    public function getTipe()
    {
        return $this->tipe;
    }

    /**
     * Set the value of tipe
     */
    public function setTipe($tipe): self
    {
        $this->tipe = $tipe;

        return $this;
    }

    /**
     * Get the value of suspensi
     */
    public function getSuspensi()
    {
        return $this->suspensi;
    }

    /**
     * Set the value of suspensi
     */
    public function setSuspensi($suspensi): self
    {
        $this->suspensi = $suspensi;

        return $this;
    }

    /**
     * Get the value of transmisi
     */
    public function getTransmisi()
    {
        return $this->transmisi;
    }

    /**
     * Set the value of transmisi
     */
    public function setTransmisi($transmisi): self
    {
        $this->transmisi = $transmisi;

        return $this;
    }
}