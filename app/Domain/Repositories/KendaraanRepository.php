<?php
namespace App\Domain\Repositories;

use App\Domain\Entity\Kendaraan;
use App\Domain\Entity\KendaraanBuilder;
use App\Models\Kendaraan as ModelsKendaraan;
use App\Utilize\ErrorException;
use Exception;

interface KendaraanRepositoryIface{
    public function findByID($id);
    public function findAll($offset,$limit);
    public function save(Kendaraan $kendaraan);
    public function update($id,Kendaraan $kendaraan);
    public function delete($id);
}


class KendaraanRepository implements KendaraanRepositoryIface{
    private static $INSTANCE=null;
    public static function getInstance(): KendaraanRepository{
        if(self::$INSTANCE==NULL):
            self::$INSTANCE= new KendaraanRepository();
        endif;
        return self::$INSTANCE;
    }
	/**
	 * @param mixed $id
	 * @return Kendaraan
	 */
	public function findByID($id) {
        $data = ModelsKendaraan::find($id);

        if($data==null){
            throw new \Exception(ErrorException::createNotFound("Rides not found"));
        }
        $data = KendaraanBuilder::fromJSON($data->toJson());
        return $data;
	}

	/**
	 *
	 * @param mixed $offset
	 * @param mixed $limit
	 * @return Kendaraan[]
	 */
	public function findAll($offset, $limit) {
        try{
            $data = ModelsKendaraan::take($limit)->skip($offset)->get();
            $result=[];
            foreach($data as $key =>$val):
                $json = json_encode($val);
                $kendaraan = KendaraanBuilder::fromJSON($json);
                $result[] = $kendaraan;
            endforeach;
            return $result;
        }catch(Exception $e){
            dd($e->getMessage());
            throw new Exception(ErrorException::createISR($e->getMessage()));
        }

	}

	/**
	 *
	 * @param Kendaraan $kendaraan
	 * @return Kendaraan
     *
     *
     * 'tahun','harga','mesin','kapasitas','type','suspensi','transmisi'
	 */
	public function save(Kendaraan $kendaraan) {
        $db = new ModelsKendaraan();
        $db->tahun = $kendaraan->tahun;
        $db->harga = $kendaraan->harga;
        $db->mesin = $kendaraan->mesin;
        $db->kapasitas = $kendaraan->kapasitas;
        $db->tipe = $kendaraan->tipe;
        $db->suspensi = $kendaraan->suspensi;
        $db->transmisi = $kendaraan->transmisi;
        $db->warna = $kendaraan->warna;
        $db->jenis = $kendaraan->jenis;
        $db->save();
        $data = KendaraanBuilder::fromJSON($db->toJson());
        return $data;
	}

	/**
	 *
	 * @param mixed $id
	 * @param Kendaraan $kendaraan
	 * @return Kendaraan
	 */
	public function update($id, Kendaraan $kendaraan) {

        $data = json_decode($kendaraan->toJSON(),true);
        unset($data['_id']);
        ModelsKendaraan::where("_id",$id)->update($data);
        $data =$this->findByID($id);
        return $data;
	}

	/**
	 *
	 * @param mixed $id
	 * @return mixed
	 */
	public function delete($id) {
        ModelsKendaraan::where("_id",$id)->delete();
        return true;
	}
}