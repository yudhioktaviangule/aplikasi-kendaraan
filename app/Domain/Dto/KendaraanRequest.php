<?php


namespace App\Domain\Dto;

class KendaraanRequest{
    public $tahun = null;
    public $warna = null;
    public $harga = null;
    public $mesin = null;



    /**
     * Get the value of tahun
     */
    public function getTahun()
    {
        return $this->tahun;
    }

    /**
     * Set the value of tahun
     */
    public function setTahun($tahun): self
    {
        $this->tahun = $tahun;

        return $this;
    }

    /**
     * Get the value of warna
     */
    public function getWarna()
    {
        return $this->warna;
    }

    /**
     * Set the value of warna
     */
    public function setWarna($warna): self
    {
        $this->warna = $warna;

        return $this;
    }

    /**
     * Get the value of harga
     */
    public function getHarga()
    {
        return $this->harga;
    }

    /**
     * Set the value of harga
     */
    public function setHarga($harga): self
    {
        $this->harga = $harga;

        return $this;
    }



    /**
     * Get the value of mesin
     */
    public function getMesin()
    {
        return $this->mesin;
    }

    /**
     * Set the value of mesin
     */
    public function setMesin($mesin): self
    {
        $this->mesin = $mesin;

        return $this;
    }
}

