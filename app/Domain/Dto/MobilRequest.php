<?php

namespace App\Domain\Dto;

class MobilRequest extends KendaraanRequest{
    public $kapasitas = null;
    public $tipe = null;


    /**
     * Get the value of kapasitas
     */
    public function getKapasitas()
    {
        return $this->kapasitas;
    }

    /**
     * Set the value of kapasitas
     */
    public function setKapasitas($kapasitas): self
    {
        $this->kapasitas = $kapasitas;

        return $this;
    }

    /**
     * Get the value of tipe
     */
    public function getTipe()
    {
        return $this->tipe;
    }

    /**
     * Set the value of tipe
     */
    public function setTipe($tipe): self
    {
        $this->tipe = $tipe;

        return $this;
    }
}