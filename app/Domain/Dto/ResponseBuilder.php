<?php

namespace App\Domain\Dto;


class ResponseBuilder{
    private $messages;
    private $datas;
    private $statuses;
    public static function builder(){
        return new ResponseBuilder();
    }
    public function message($message){
        $this->messages = $message;
        return $this;
    }
    public function data($data){
        $this->datas = $data;
        return $this;
    }
    public function status($status){
        $this->statuses = $status;
        return $this;
    }

    public function build():Response{
        return new Response($this->messages,$this->datas,$this->statuses);
    }
}