<?php
namespace App\Domain\Dto;
class MotorRequest extends KendaraanRequest{
    public $suspensi = null;
    public $transmisi = null;

    /**
     * Get the value of suspensi
     */
    public function getSuspensi()
    {
        return $this->suspensi;
    }

    /**
     * Set the value of suspensi
     */
    public function setSuspensi($suspensi): self
    {
        $this->suspensi = $suspensi;

        return $this;
    }

    /**
     * Get the value of transmisi
     */
    public function getTransmisi()
    {
        return $this->transmisi;
    }

    /**
     * Set the value of transmisi
     */
    public function setTransmisi($transmisi): self
    {
        $this->transmisi = $transmisi;

        return $this;
    }
}