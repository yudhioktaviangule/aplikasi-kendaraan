<?php

 namespace App\Domain\Services;

use App\Domain\Dto\KendaraanRequest;
use App\Domain\Dto\MobilRequest;
use App\Domain\Dto\MotorRequest;
use App\Domain\Dto\Response;
use App\Domain\Dto\ResponseBuilder;
use App\Domain\Entity\Kendaraan;
use App\Domain\Entity\KendaraanBuilder;
use App\Domain\Repositories\KendaraanRepository;
use App\Utilize\ErrorException;
use App\Utilize\Pagination;
use App\Utilize\Validation;
use Exception;
use Illuminate\Http\Request as HttpRequest;

class KendaraanServiceIface{
            #code here
}


class KendaraanService{

    private $repo = null;
    private static  $INSTANCE=null;
    public static function getInstance(): KendaraanService {
        if(self::$INSTANCE==NULL):
            self::$INSTANCE= new KendaraanService();
        endif;
        return self::$INSTANCE;
    }
    public function __construct() {
        $this->repo = KendaraanRepository::getInstance();
    }

    public function findAll($page):Response{
        try{
            Validation::paramCheck("page",$page);
            $db       = $this->repo;
            $pageable = Pagination::getInstance();
            $paging   = $pageable->process($page ?? "0");
            $data     = $db->findAll($paging->offset,$paging->limit);
            return ResponseBuilder::builder()->message("OK")->data($data)->status(200)->build();
        }catch(\Exception $e){
            return $this->createError($e);
        }
    }

    public function findByID($id=""):Response
    {
        try{
            Validation::paramCheck("id",$id);
            $db       = $this->repo;
            $pageable = Pagination::getInstance();
            $data     = $db->findByID($id);
            return ResponseBuilder::builder()->message("OK")->data($data)->status(200)->build();
        }catch(\Exception $e){
           return $this->createError($e);
        }
    }
    public function saveMotor(MotorRequest $kendaraan):Response
    {
        try{
            Validation::paramMinusCheck("tahun",$kendaraan->getTahun());
            Validation::paramMinusCheck("harga",$kendaraan->getHarga());
            Validation::paramCheck("warna",$kendaraan->getWarna());
            Validation::paramCheck("mesin",$kendaraan->getMesin());
            Validation::paramCheck("suspensi",$kendaraan->getSuspensi());
            Validation::paramCheck("transmisi",$kendaraan->getTransmisi());

            $motor = json_decode(json_encode($kendaraan),true);
            $motor['jenis'] = 'motor';
            $repo =  KendaraanRepository::getInstance();
            $dataKendaraan = KendaraanBuilder::fromJSON(json_encode($motor));
            $data = $repo->save($dataKendaraan);
            return ResponseBuilder::builder()->message("OK")->data($data)->status(201)->build();
        }catch(\Exception $e){
           return $this->createError($e);
        }
    }
    public function saveMobil(MobilRequest $kendaraan):Response
    {
        try{

            Validation::paramCheck("warna",$kendaraan->getWarna());
            Validation::paramCheck("mesin",$kendaraan->getMesin());
            Validation::paramCheck("kapasitas",$kendaraan->getKapasitas());
            Validation::paramCheck("tipe",$kendaraan->getTipe());
            Validation::paramYearCheck('tahun',$kendaraan->getTahun());
            Validation::paramMinusCheck('harga',$kendaraan->getHarga());
            $repo =  KendaraanRepository::getInstance();

            $mobil = json_decode(json_encode($kendaraan),true);
            $mobil['jenis'] = 'mobil';
            $dataKendaraan = KendaraanBuilder::fromJSON(json_encode($mobil));
            $data = $repo->save($dataKendaraan);
            return ResponseBuilder::builder()->message("OK")->data($data)->status(201)->build();
        }catch(\Exception $e){
           return $this->createError($e);
        }
    }


    public function updateMotor($id=NULL, MotorRequest $kendaraan){
        try{
            Validation::paramCheck("id",$id);
            if($kendaraan->getTahun()!=null){
                Validation::paramYearCheck("tahun",$kendaraan->getTahun());
            }
            if($kendaraan->getHarga()!=null){
                Validation::paramMinusCheck("harga",$kendaraan->getHarga());
            }
            $repo =  KendaraanRepository::getInstance();
            $data = $repo->findByID($id);
            if($data->getJenis()!=='motor'):
                throw new \Exception(ErrorException::createBADRequest("Gagal mengupdate : Type dari $id adalah {$data->getJenis()}"));
            endif;
            $kendaraans = json_decode(json_encode($kendaraan),TRUE);
            $kendaraans['jenis']=$data->getJenis();

            $dataKendaraan = KendaraanBuilder::fromJSON(json_encode($kendaraans));
            $data = $repo->update($id,$dataKendaraan);
            return ResponseBuilder::builder()->message("OK")->data($data)->status(200)->build();
        }catch(\Exception $e){
           return $this->createError($e);
        }
    }
    public function updateMobil($id=NULL, MobilRequest $kendaraan){
        try{
            Validation::paramCheck("id",$id);
            if($kendaraan->getTahun()!=null){
                Validation::paramYearCheck("tahun",$kendaraan->getTahun());
            }
            if($kendaraan->getHarga()!=null){
                Validation::paramMinusCheck("harga",$kendaraan->getHarga());
            }
            $repo =  KendaraanRepository::getInstance();
            $data = $repo->findByID($id);
            if($data->getJenis()!=='mobil'):
                throw new \Exception(ErrorException::createBADRequest("Gagal mengupdate, Type dari $id adalah {$data->getJenis()}"));
            endif;


            $kendaraans = json_decode(json_encode($kendaraan),TRUE);
            $kendaraans['jenis']=$data->getJenis();
            $dataKendaraan = KendaraanBuilder::fromJSON(json_encode($kendaraans));
            $data = $repo->update($id,$dataKendaraan);
            return ResponseBuilder::builder()->message("OK")->data($data)->status(200)->build();
        }catch(\Exception $e){
           return $this->createError($e);
        }
    }

    public function deleteData($id){
        try {
            $repo = $this->repo;
            Validation::paramCheck("id",$id);
            $repo->findByID($id);
            $repo->delete($id);
            return ResponseBuilder::builder()->message("OK")->data(null)->status(200)->build();
        } catch (\Exception $e) {
            return $this->createError($e);
        }

    }

    public function createError(\Exception $e){
        $err = ErrorException::getResponseStatusCode($e->getMessage());
        return ResponseBuilder::builder()->message($err->err)->data(null)->status($err->code)->build();
    }
}